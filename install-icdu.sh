#!/bin/bash

PACKAGING_TMP_LOCATION=/var/tmp/icdu-packaging

PSQL_VERSION=$(psql --version | grep -oP -m 1 'psql.+ \K\d+\.\d+')

# Check PostgreSQL version
if [[ "$PSQL_VERSION" < "9.5" ]]; then
    echo "PostgreSQL 9.5 or newer needs to be installed (see https://wiki.postgresql.org/wiki/Apt)"
    exit 2
fi

echo "=== XiVO Centralized Interface installation ==="
# Get packaging project
git clone https://gitlab.com/xivo-utils/icdu-packaging.git $PACKAGING_TMP_LOCATION

# Copy configuration files to the right locations
mkdir -p /etc/docker
cp -r $PACKAGING_TMP_LOCATION/compose $PACKAGING_TMP_LOCATION/interface-centralisee $PACKAGING_TMP_LOCATION/routing-server /etc/docker

# Generate a SSH key
echo "=== Creation of an SSH key for XCI ==="
echo "* Be sure to leave the key passphrase empty!"
ssh-keygen -t rsa -f /etc/docker/interface-centralisee/ssh_key

# Configure PostgreSQL
echo "=== Configuration of the PostgreSQL database ==="
apt-get install -y postgresql-contrib-$PSQL_VERSION
sudo -u postgres psql -c "CREATE USER icx WITH PASSWORD 'icx'"
sudo -u postgres psql -c "CREATE DATABASE icx WITH OWNER icx"
sudo -u postgres psql icx -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp";'

# Remove temp files
rm -r $PACKAGING_TMP_LOCATION

echo "XiVO Centralized Interface is now installed. Remember to consult the documentation available on https://documentation.xivo.solutions"

