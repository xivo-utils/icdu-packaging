#!/usr/bin/python
# -*- coding: UTF-8 -*-
# Copyright (C) 2012  Avencall
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,

import argparse
import logging
import sys
import traceback
import requests
import ConfigParser

from xivo.agi import AGI

#
# User configuration
# -------------

DEBUG_MODE = True # False 
LOGFILE = '/var/log/asterisk/xivo-routage-agi.log'
CONFIG_FILE = '/etc/xivo_routage.conf'
#
# Factory configuration
# -------------

GENERAL_SECTION = 'general'
HOSTS_OPTION = 'hosts'
URL_HEADERS = { 'User-Agent' : 'XiVO Webservice AGI' }
CONNECTION_TIMEOUT = 2

# Without server address/name
REQUEST_URL = 'route'
PARAMETER_NAME = 'digits'


agi = AGI()
logger = logging.getLogger()


class Syslogger(object):

    def write(self, data):
        global logger
        logger.error(data)


def init_logging(debug_mode):
    if debug_mode:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logfilehandler = logging.FileHandler(LOGFILE)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    logfilehandler.setFormatter(formatter)
    logger.addHandler(logfilehandler)

    syslogger = Syslogger()
    sys.stderr = syslogger


def get_configured_hosts():
    config = ConfigParser.ConfigParser()
    config.read(CONFIG_FILE)
    hosts_str = config.get(GENERAL_SECTION, HOSTS_OPTION)
    return hosts_str.split(',')


def get_routes(digits):
    params = {PARAMETER_NAME: digits}

    i = 0
    get_routes_request = None
    for ip in get_configured_hosts():
        logger.debug("Getting routes for " + str(digits) + " from " + ip)
        get_routes_request = None
        try:
            get_routes_request = requests.get("http://" + ip + "/" + REQUEST_URL,
                                              params=params,
                                              timeout=CONNECTION_TIMEOUT)

        except requests.exceptions.ConnectionError:
            logger.warning("Server " + ip + " refused connection")
            xc_error_cause = "Refused connection from WS: " + ip

        except ValueError:
            logger.error("No JSON encoded response from " + ip)
            xc_error_cause = "No JSON encoded response from " + ip

        except requests.exceptions.Timeout:
            logger.error("Timeout when connecting to the following server " + ip)
            xc_error_cause = "Timeout when connecting to the following server " + ip

        except:
            logger.error("Unknown problem during connection to the following server: " + ip)
            exc_type, exc_value, exc_traceback = sys.exc_info()
            logger.debug(repr(traceback.format_exception(exc_type, exc_value,
                                              exc_traceback)))
            xc_error_cause = "Connection failed to WS"

        if get_routes_request is not None:
            if get_routes_request.status_code != requests.codes.ok:
                if get_routes_request.status_code == requests.codes.not_found:
                    xc_error_cause = "No entry found for " + str(digits) + " on server " + ip
                    logger.warning("No entry found for " + str(digits) + " on server " + ip)
                    process_no_response(digits, xc_error_cause)
                else:
                    logger.error("Server " + ip + " returned following error: " + str(get_routes_request.status_code))
                    xc_error_cause = "Server " + ip + " returned following error: " + str(get_routes_request.status_code)

            else:
                routes = get_routes_request.json()
                logger.info("Got following routes for digits: " + str(digits) + ": " + str(routes))
                return routes
        else:
            if xc_error_cause is None:
                xc_error_cause = "Unknown problem during connection to the following server: " + ip
                logger.error("Unknown problem during connection to the following server: " + ip)

    process_no_response(digits, xc_error_cause)


def set_dialplan_variables(routes):
    logger.debug("Setting dialplan variables using: " + str(routes))
    agi.set_variable("xc_digits", routes['digits'])
    agi.set_variable("xc_regexp", routes['regexp'])
    agi.set_variable("xc_target", routes['target'])
    agi.set_variable("xc_context", routes['context'])


#Timeout & tous les cas ou on n'a pas de réponse
def process_no_response(digits, xc_error_cause):
    logger.warning("Got no response for digits: " + str(digits) + "!")
    agi.set_variable("xc_error_cause", xc_error_cause)
    sys.exit(1)


def process_request(digits):
    routes = get_routes(digits)
    set_dialplan_variables(routes)


def main():
    init_logging(DEBUG_MODE)
    try:
        if len(sys.argv) < 1:
            logger.error("wrong number of arguments")
            sys.exit(1)
        process_request(sys.argv[1])
    except Exception:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        logger.error(repr(traceback.format_exception(exc_type, exc_value,
                                          exc_traceback)))
        sys.exit(1)

    sys.exit(0)


if __name__ == '__main__':
    main()
